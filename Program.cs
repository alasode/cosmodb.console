﻿using System;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using Microsoft.Azure.Cosmos;
using System.Diagnostics;
using CosmoDB.Console.Model;


namespace CosmoDB.Console
{
    class Program
    {
        //private string EndpointUrl = Environment.GetEnvironmentVariable("EndpointUrl");
        //private string PrimaryKey = Environment.GetEnvironmentVariable("PrimaryKey");


        public static async Task Main(string[] args)
        {
            string EndpointUrl = Environment.GetEnvironmentVariable("EndpointUrl");
            string PrimaryKey = Environment.GetEnvironmentVariable("PrimaryKey");
            CosmosClient CosmosClient = new CosmosClient(EndpointUrl, PrimaryKey);

            var database = CosmosClient.GetDatabase("ChampionProductCosmoDB");
            var table = CosmosClient.GetContainer(database.Id, "Customer2");

            try
            {
                Program p = new Program();
                //await p.QueryItemsAsync(table);
                await p.CreateNewCustomer(table);
                //await p.UpdateItemAsync(table);
                //await p.DeleteItemAsync(table);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: {0}", e);
            }
        }

        private async Task QueryItemsAsync(Container table)
        {
            var sqlQueryText = "SELECT * FROM c";

            Debug.WriteLine("Running query: {0}\n", sqlQueryText);

            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Customer> queryResultSet = table.GetItemQueryIterator<Customer>(queryDefinition);

            while (queryResultSet.HasMoreResults)
            {
                FeedResponse<Customer> currentResultSet = await queryResultSet.ReadNextAsync();
                foreach (Customer customer in currentResultSet)
                {
                    Debug.WriteLine("Name: " + customer.lastName + ", " + customer.firstName);
                }
            }

            //Get records with 'Smith' as lastname
            sqlQueryText = "SELECT * FROM c where c.lastName = 'Smith'";
            queryDefinition = new QueryDefinition(sqlQueryText);
            queryResultSet = table.GetItemQueryIterator<Customer>(sqlQueryText);

            FeedResponse<Customer> resultSet = Task.Run(() => queryResultSet.ReadNextAsync()).GetAwaiter().GetResult();

            foreach (var item in resultSet.Resource)
            {
                Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
            }
        }

        private async Task CreateNewCustomer(Container table)
        {
            var service = new CosmoDB.Console.Service.CustomerService();
            var getCustService = new CosmoDB.Console.Service.GetCustomerService();

            var newCustomer1 = getCustService.GetCustomer1();
            var newCustomer2 = getCustService.GetCustomer2();
            var newCustomer3 = getCustService.GetCustomer3();

            var wasSuccessfullyAdded = Task.Run(() => service.CreateCustomerAsync(table, newCustomer1)).GetAwaiter().GetResult();
                wasSuccessfullyAdded = Task.Run(() => service.CreateCustomerAsync(table, newCustomer2)).GetAwaiter().GetResult();
                wasSuccessfullyAdded = Task.Run(() => service.CreateCustomerAsync(table, newCustomer3)).GetAwaiter().GetResult();
        }

        private async Task UpdateItemAsync(Container table)
        {
            var Service = new CosmoDB.Console.Service.CustomerService();
            Customer customerToUpdate = await table.ReadItemAsync<Customer>("1", new PartitionKey("1"));

            if (customerToUpdate != null)
            {
                customerToUpdate.firstName = "Peter"; //make a change to any property in the customer object
                //customerToUpdate..Fir; //make a change to any property in the customer object

                var wasSuccessfullyUpdated = Task.Run(() => Service.UpdateCustomerAsync(table, customerToUpdate)).GetAwaiter().GetResult();
            }
        }

        private async Task DeleteItemAsync(Container table)
        {
            var Service = new CosmoDB.Console.Service.CustomerService();
            Customer customerToDelete = await table.ReadItemAsync<Customer>("1", new PartitionKey("1"));

            var wasSuccessfullyDeleted = Task.Run(() => Service.DeleteCustomerAsync(table, customerToDelete)).GetAwaiter().GetResult();
        }


        }
    }

