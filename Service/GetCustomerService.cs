﻿using CosmoDB.Console.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CosmoDB.Console.Service
{
    public class GetCustomerService
    {
    
        public Customer GetCustomer1()
        {
            var customer1 = new Customer()
            {
                id = "1",
                customerId = 1,
                lastName = "Smith",
                firstName = "Tabb",
                addresses = new List<Address>() {
                                                    new Address()
                                                    {
                                                        addressId=1,
                                                        addressTypeId = 1,
                                                        street1 = "100 Main Street",
                                                        street2 = "Suite 2",
                                                        city = "Dallas",
                                                        state = "TX",
                                                        zip = "75201"
                                                     },
                                                    new Address()
                                                    {
                                                        addressId=2,
                                                        addressTypeId = 2,
                                                        street1 = "P.O. Box 123",
                                                        street2 = null,
                                                        city = "Dallas",
                                                        state = "TX",
                                                        zip = "75202"
                                                    }
                    },
                invoices = new List<Invoice>() {new Invoice()
                                                    {
                                                        invoiceId = 1,
                                                        date = DateTime.Parse("2019-04-28"),
                                                        amount = decimal.Parse("101.25"),
                                                        paidDate = null,
                                                        paidAmount = 0,
                                                    },
                                                    new Invoice()
                                                    {
                                                        invoiceId = 2,
                                                        date = DateTime.Parse("2019-05-01"),
                                                        amount = decimal.Parse("23.54"),
                                                        paidDate = DateTime.Parse("2019-05-02"),
                                                        paidAmount = decimal.Parse("23.54")
                                                    }

                    }
            };
            return customer1;
        }
        public Customer GetCustomer2()
            {
            var customer2 = new Customer()
            {
                id = "2",
                customerId = 2,
                lastName = "Jones",
                firstName = "Tammy",
                addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=3,
                                                    addressTypeId = 1,
                                                    street1 = "111 Main Street",
                                                    street2 = "Suite 25",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    },
                                                new Address()
                                                {
                                                    addressId=4,
                                                    addressTypeId = 2,
                                                    street1 = "P.O. Box 123445",
                                                    street2 = null,
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75202"
                                                }
                },
                invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 3,
                                                    date = DateTime.Parse("2019-02-28"),
                                                    amount = decimal.Parse("121.25"),
                                                    paidDate = null,
                                                    paidAmount = 10,
                                                },
                                                new Invoice()
                                                {
                                                    invoiceId = 4,
                                                    date = DateTime.Parse("2019-04-01"),
                                                    amount = decimal.Parse("223.54"),
                                                    paidDate = DateTime.Parse("2019-05-02"),
                                                    paidAmount = decimal.Parse("223.54")
                                                }

                }
            };
            return customer2;
            }

        public Customer GetCustomer3()
        {
            var customer3 = new Customer()
            {
                id = "3",
                customerId = 3,
                lastName = "Struck",
                firstName = "Charles",
                addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=5,
                                                    addressTypeId = 1,
                                                    street1 = "3000 Lewis Drive",
                                                    street2 = "Suite 200",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    }

                },
                invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 5,
                                                    date = DateTime.Parse("2019-02-20"),
                                                    amount = decimal.Parse("1121.25"),
                                                    paidDate = null,
                                                    paidAmount = decimal.Parse("100.22"),
                                                }

                }
            };
            return customer3;
        }
    }
}
