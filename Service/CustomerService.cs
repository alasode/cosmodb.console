﻿using CosmoDB.Console.Model;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmoDB.Console.Service
{
    public class CustomerService
    {

        public CustomerService()
        {

        }

        public async Task<Boolean> CreateCustomerAsync(Container container, Customer newCustomer)
        {
            try
            {
                await container.CreateItemAsync(newCustomer, new PartitionKey(newCustomer.id));
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<Boolean> UpdateCustomerAsync(Container container, Customer existingCustomer)
        {
            try
            {
                await container.ReplaceItemAsync(existingCustomer,existingCustomer.customerId.ToString(), new PartitionKey(existingCustomer.id));
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<Boolean> DeleteCustomerAsync(Container container, Customer existingCustomer)
        {
            try
            {
                await container.DeleteItemAsync<Customer>(existingCustomer.customerId.ToString(), new PartitionKey(existingCustomer.id));
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }
    }
}